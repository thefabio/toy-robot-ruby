require_relative 'position'

class RobotCommand
  PLACE = 'PLACE'
  MOVE = 'MOVE'
  LEFT = 'LEFT'
  RIGHT = 'RIGHT'
  REPORT = 'REPORT'

  attr_reader :errors

  def initialize(position, command_line)
    @errors = []
    @all_commands = [PLACE, MOVE, LEFT, RIGHT, REPORT]

    @command_line = command_line
    @position = position

    validate
  end

  def execute
    return @position if @errors.any?

    command_parts = @command_line.split(' ')

    case command_parts[0]
      when LEFT
        left
      when RIGHT
        right
      when MOVE
        move
      when REPORT
        report
      when PLACE
        place command_parts[1]
      else
        #nothing to do, already validate
    end
  end

  private
  def validate
    command_parts = @command_line.split(' ')

    if command_parts.length == 0
      @errors.push ("invalid command line: '#{@command_line}'")
      return
    end

    @errors.push ("Command does not exist: '#{@command_line}', valid commands are #{@all_commands.join(', ')}") unless @all_commands.include? command_parts[0]

    case command_parts[0]
      when MOVE, LEFT, RIGHT, REPORT
        @errors.push ("Command line: '#{@command_line}' has too many parts, should be one of #{[MOVE, LEFT, RIGHT, REPORT].join(', ')}") unless command_parts.length == 1
      when PLACE
        @errors.push ("Command line: '#{@command_line}' has invalid parts, should be in the format: 'PLACE X,Y,FACING'") unless command_parts.length == 2
        validate_place_command command_parts[1] if command_parts.length == 2
      else
        #nothing to do as already validated
    end

    if @position.nil? && command_parts[0] != PLACE
      @errors.push "A #{PLACE} command needs to be run before #{command_parts[0]} can be executed"

    elsif !@position.nil? && @position.errors.any?
      @position.errors.each do |e|
        @errors.push e
      end
    end
  end

  def validate_place_command (place_command_line)
    place_command_line_parts = place_command_line.split(',')

    if place_command_line_parts.length == 3
      new_position = Position.new(place_command_line_parts[0],place_command_line_parts[1], place_command_line_parts[2])
      new_position.errors.each do |e|
        @errors.push e
      end
    else
      @errors.push ("PLACE command attributes are invalid: '#{place_command_line}', should be in the format: 'PLACE X,Y,FACING'")
    end
  end


  def left
    coord_x = @position.x
    coord_y = @position.y
    facing = @position.direction.value

    case facing
      when Direction::NORTH
        facing = Direction::WEST
      when Direction::WEST
        facing = Direction::SOUTH
      when Direction::SOUTH
        facing = Direction::EAST
      when Direction::EAST
        facing = Direction::NORTH

      else
        facing = ''
    end

    Position.new(coord_x, coord_y, facing)
  end

  def right
    coord_x = @position.x
    coord_y = @position.y
    facing = @position.direction.value

    case facing
      when Direction::NORTH
        facing = Direction::EAST
      when Direction::EAST
        facing = Direction::SOUTH
      when Direction::SOUTH
        facing = Direction::WEST
      when Direction::WEST
        facing = Direction::NORTH
      else
        facing = ''
    end

    Position.new(coord_x, coord_y, facing)
  end

  def move
    coord_x = @position.x
    coord_y = @position.y
    facing = @position.direction.value

    case facing
      when Direction::NORTH
        coord_y += 1
      when Direction::EAST
        coord_x += 1
      when Direction::SOUTH
        coord_y -= 1
      when Direction::WEST
        coord_x -= 1
      else
        #nothing to do
    end

    Position.new(coord_x, coord_y, facing)
  end

  def report
    puts "#{@position.x},#{@position.y},#{@position.direction.value}"

    Position.new(@position.x,@position.y,@position.direction.value)
  end

  def place (place_command_line)
    place_command_line_parts = place_command_line.split(',')

    Position.new(place_command_line_parts[0], place_command_line_parts[1], place_command_line_parts[2])
  end

end