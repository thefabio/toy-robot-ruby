require_relative 'robot_command'

class Robot
  attr_reader :current_position

  def initialize
    @current_position = nil
  end

  def execute_command(command_line)
    robot_command = RobotCommand.new(@current_position,command_line)
    if robot_command.errors.any?
      robot_command.errors.each do |e|
        puts e
      end
    else
      new_position = robot_command.execute
      if new_position.x < 0 ||
          new_position.y < 0 ||
          new_position.x > 4 ||
          new_position.y > 4

        puts "Robot refuses to move. The position (#{new_position.x},#{new_position.y}) is Out of the table"
      else
        @current_position = new_position
      end
    end
  end
end