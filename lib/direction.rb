class Direction
  NORTH = 'NORTH'
  SOUTH = 'SOUTH'
  EAST = 'EAST'
  WEST = 'WEST'

  attr_reader :errors

  def initialize(direction_literal)
    @all_directions = [NORTH, SOUTH, EAST, WEST]
    @errors = []

    @direction_literal = direction_literal
    validate
  end

  def value
    @direction_literal
  end

  private
  def validate
    unless @all_directions.include? @direction_literal
      @errors.push("Invalid Direction: #{@direction_literal}")
    end
  end
end