require_relative 'direction'
class Position
  attr_reader :x
  attr_reader :y
  attr_reader :direction
  attr_reader :errors

  def initialize(x, y, direction_literal)
    @errors = []

    if x.class == String && x.match(/^\d+$/)
      @x=x.to_i
    elsif x.class == Fixnum
      @x=x
    else
      @errors.push("invalid X coordinate: #{x}")
    end

    if y.class == String && y.match(/^\d+$/)
      @y=y.to_i
    elsif y.class == Fixnum
      @y=y
    else
      @errors.push("invalid Y coordinate: #{y}")
    end

    @direction = Direction.new(direction_literal)

    validate
  end

  private
  def validate
    if @direction.errors.any?
      @direction.errors.each do |e|
        @errors.push(e)
      end
    end
  end
end