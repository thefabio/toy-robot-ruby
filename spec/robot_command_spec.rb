require 'spec_helper'

RSpec.describe RobotCommand do

  before(:each) do
    @position = Position.new(1,2,Direction::EAST)
  end

  it 'Should error if invalid command line is sent' do
    new_command = RobotCommand.new(@position, 'move')
    new_command2 = RobotCommand.new(@position, 'inv')

    expect(new_command.errors.any?).to be_truthy
    expect(new_command2.errors.any?).to be_truthy
  end

  it 'Should move left when command LEFT is sent' do
    new_command = RobotCommand.new(@position, 'LEFT')
    new_position = new_command.execute

    expect(new_command.errors.any?).to be_falsey
    expect(new_position.x).to eq(1)
    expect(new_position.y).to eq(2)
    expect(new_position.direction.value).to eq(Direction::NORTH)
  end

  it 'Should move right when command RIGHT is sent' do
    new_command = RobotCommand.new(@position, 'RIGHT')
    new_position = new_command.execute

    expect(new_command.errors.any?).to be_falsey
    expect(new_position.x).to eq(1)
    expect(new_position.y).to eq(2)
    expect(new_position.direction.value).to eq(Direction::SOUTH)
  end

  it 'Should move EAST when command MOVE is sent' do
    new_command = RobotCommand.new(@position, 'MOVE')
    new_position = new_command.execute

    expect(new_command.errors.any?).to be_falsey
    expect(new_position.x).to eq(2)
    expect(new_position.y).to eq(2)
    expect(new_position.direction.value).to eq(Direction::EAST)
  end

  it 'Should not move when command REPORT is sent' do
    new_command = RobotCommand.new(@position, 'REPORT')
    new_position = new_command.execute

    expect(new_command.errors.any?).to be_falsey
    expect(new_position.x).to eq(1)
    expect(new_position.y).to eq(2)
    expect(new_position.direction.value).to be(Direction::EAST)
  end

  it 'Should error if PLACE command is not executed first' do
    new_command = RobotCommand.new(nil, RobotCommand::MOVE)
    new_command1 = RobotCommand.new(nil, RobotCommand::LEFT)
    new_command2 = RobotCommand.new(nil, RobotCommand::RIGHT)
    new_command3 = RobotCommand.new(nil, RobotCommand::REPORT)

    expect(new_command.errors.any?).to be_truthy
    expect(new_command1.errors.any?).to be_truthy
    expect(new_command2.errors.any?).to be_truthy
    expect(new_command3.errors.any?).to be_truthy
  end

  it 'Should execute PLACE command' do
    new_command = RobotCommand.new(nil, "PLACE 3,2,WEST")
    new_position = new_command.execute

    expect(new_command.errors.any?).to be_falsey
    expect(new_position.x).to eq(3)
    expect(new_position.y).to eq(2)
    expect(new_position.direction.value).to eq(Direction::WEST)
  end

end