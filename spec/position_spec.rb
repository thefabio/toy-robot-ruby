require 'spec_helper'

RSpec.describe Position do

  it 'Should error if invalid direction is set' do
    new_position = Position.new(0, 0, 'Error')
    new_position2 = Position.new(0, 0, 'north')

    expect(new_position.errors.any?).to be_truthy
    expect(new_position2.errors.any?).to be_truthy
  end

  it 'Should error if non numeric character is set on coordinates' do
    new_position = Position.new(',', 0, Direction::WEST)
    new_position2 = Position.new(0, ',', Direction::WEST)
    new_position3= Position.new('%', 0, Direction::WEST)
    new_position4 = Position.new(0, '%', Direction::WEST)

    expect(new_position.errors.any?).to be_truthy
    expect(new_position2.errors.any?).to be_truthy
    expect(new_position3.errors.any?).to be_truthy
    expect(new_position4.errors.any?).to be_truthy
  end

  it 'Should not error if  numeric character is set on coordinates' do
    new_position = Position.new('0', '1', Direction::WEST)
    new_position2 = Position.new(0, '2', Direction::WEST)

    expect(new_position.errors.any?).to be_falsey
    expect(new_position2.errors.any?).to be_falsey
  end

end