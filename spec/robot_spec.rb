require 'spec_helper'

RSpec.describe Robot, 'Simulator pdf examples' do
  before(:each) do
    @robot = Robot.new
  end

  it 'should have results from Example 01' do
    @robot.execute_command('PLACE 0,0,NORTH')
    @robot.execute_command('MOVE')
    @robot.execute_command('REPORT')

    expect(@robot.current_position.x).to eq(0)
    expect(@robot.current_position.y).to eq(1)
    expect(@robot.current_position.direction.value).to eq(Direction::NORTH)
  end

  it 'should have results from Example 02' do
    @robot.execute_command('PLACE 0,0,NORTH')
    @robot.execute_command('LEFT')
    @robot.execute_command('REPORT')

    expect(@robot.current_position.x).to eq(0)
    expect(@robot.current_position.y).to eq(0)
    expect(@robot.current_position.direction.value).to eq(Direction::WEST)
  end

  it 'should have results from Example 03' do
    @robot.execute_command('PLACE 1,2,EAST')
    @robot.execute_command('MOVE')
    @robot.execute_command('MOVE')
    @robot.execute_command('LEFT')
    @robot.execute_command('MOVE')
    @robot.execute_command('REPORT')

    expect(@robot.current_position.x).to eq(3)
    expect(@robot.current_position.y).to eq(3)
    expect(@robot.current_position.direction.value).to eq(Direction::NORTH)
  end
end