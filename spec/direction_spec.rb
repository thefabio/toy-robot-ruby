require 'spec_helper'

RSpec.describe Direction do

  it 'Should error if invalid direction is set' do
    new_direction = Direction.new('Error')
    new_direction2 = Direction.new('north')

    expect(new_direction.errors.any?).to be_truthy
    expect(new_direction2.errors.any?).to be_truthy
  end

  it 'Should not error if valid direction is set' do
    new_direction = Direction.new(Direction::WEST)
    new_direction2 = Direction.new(Direction::EAST)
    new_direction3 = Direction.new(Direction::NORTH)
    new_direction4 = Direction.new(Direction::SOUTH)

    expect(new_direction.errors.any?).to be_falsey
    expect(new_direction2.errors.any?).to be_falsey
    expect(new_direction3.errors.any?).to be_falsey
    expect(new_direction4.errors.any?).to be_falsey
  end

end