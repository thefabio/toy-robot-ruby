require_relative './lib/robot'

robot = Robot.new
puts 'Robot Simulator'
puts 'Please enter one of the following commands (the first command must be a PLACE):'
puts '   PLACE X,Y,NORTH|SOUTH|EAST|WEST'
puts '   MOVE'
puts '   LEFT'
puts '   RIGHT'
puts '   REPORT'

loop do
  read_command = STDIN.gets
  read_command.strip!
  robot.execute_command(read_command)
end
