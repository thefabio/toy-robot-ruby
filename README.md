ToyRobot Simulation
==================================

This is a response to the code challenge described at [ToyRobot.md](ToyRobot.md)
## Setting up
* Verify that you are running at least Ruby 2.2.2 by running `ruby -v` terminal/console window

From the application root folder execute the following to install dependencies locally
```
bundle install
```

## Starting the Robot Simulation
From the application root folder execute the command:
```
ruby simulation.rb
```

## Running unit tests

From the application root folder execute the command `rspec`
